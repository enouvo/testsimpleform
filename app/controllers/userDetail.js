var args = arguments[0] || {};
console.log(args);
function attach(){
	$.detailUsername.text = args.username;
	$.detailPass.text = args.password;
	$.detailFullname.text = args.fullname;
	$.detailMobile.text = args.mobile;
	$.detailEmail.text = args.email;
	$.detailAddress.text = args.address;
	$.detailCompany.text = args.company;
	$.btnFooter.addEventListener('click',detach);
}

function detach(){
	args = null;
	$.destroy();
	$.off();
	Alloy.createController('register').getView().open();
	$.detailWin.close();
	$.btnFooter.removeEventListener('click',detach);
}

exports.attach = attach;
exports.dettach = detach;