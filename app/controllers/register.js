var args = arguments[0] || {};
$.win.addEventListener('open', attach);

function attach() {
    $.btnFooter.addEventListener('click', register);
}


function register() {

    if (validateInputs()) {
        var userData = {
            username: $.detailUsername.getValue(),
            password: $.detailPass.getValue(),
            fullname: $.detailFullname.getValue(),
            mobile: $.detailMobile.getValue(),
            email: $.detailEmail.getValue(),
            address: $.detailAddress.getValue(),
            company: $.detailCompany.getValue(),
        }
        var model = Alloy.createModel("userData");
        model.signUp(userData, function(data, err) {
            if (err) {
                console.log(err);

            } else {                
                var userDetailController = Alloy.createController('userDetail', data);
                userDetailController.attach();
                userDetailController.getView().open();
                userDetailController = null;
                userData = null;
                detach();
            }
        });
        model = null;
    } else {
        Alloy.createWidget('nl.fokkezb.toast', {
            message: "Please enter all the required fields...",
            theme: 'error',
            duration: 3000,
        });        
    }
}

function validateInputs(){
    return  validateControl($.detailUsername) 
            & validateControl($.detailPass) 
            & validateControl($.detailEmail)
            & validateEmail($.detailEmail.getValue());
}

function validateControl(control){
    if (_.isEmpty(control.getValue())) {
        control.borderColor = Alloy.Globals.COLORS.RED_LIGHT;
        return false;
    } else {
        control.borderColor = Alloy.Globals.COLORS.GRAY_LIGHT;
        return true;
    }
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function detach() {    
    $.destroy();
    $.off();
    $.win.removeEventListener('open', attach);
    $.btnFooter.removeEventListener('click', register);
    $.win.close();
}

exports.attach = attach;
exports.detach = detach;
