exports.definition = {
	config: {
		"adapter": {
			"type": "restapi",
			"collection_name": "userData",
			"idAttribute": "objectId"
		},
		 "headers": { // your custom headers
		    "Accept": "application/json",
			"X-Parse-Application-Id":"18Qwm7uJ2lFUo2k2kEPQyDXwzLmci9QxP2NK0yJa",
			"X-Parse-REST-API-Key": "ln97qSca2PJsNg5OJgDXORBWiOJ71G7eY1nxDxHw"
		},
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
			signUp : function (userData, callback) {
				Ti.API.info('userData');
				Ti.API.info(userData);
				var _this = this;
				Ti.API.info('this Object');
				Ti.API.info(this);
				this.save(userData, {
					url : "https://api.parse.com/1/classes/Users",
					success : function () {
						Ti.API.info('signUp success.');
						var user = _this.toJSON();
						Ti.API.info('user');
						Ti.API.info(user);
							if (callback) callback(user, null);
					},
					error : function (data, err) {
						Ti.API.info('error function:');
						Ti.API.info(data);
						Ti.API.info('signin failed');
						Ti.API.info('err: '+ err);	
						if (callback) callback(null, err);
					}
				});
			},
			
		});

		return Model;
	},
	extendCollection: function(Collection) {
		_.extend(Collection.prototype, {

		});

		return Collection;
	}
};